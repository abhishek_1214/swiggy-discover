import * as React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import HomePage from './pages/home';

const PublicRoutes = () => (
  <Router>
    <Switch>
      <Route exact={true} path={'/'} component={HomePage} />
    </Switch>
  </Router>
);

export default PublicRoutes;