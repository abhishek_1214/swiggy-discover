import * as React from 'react';
import { StyleRulesCallback, WithStyles, withStyles, Input, IconButton, Icon } from 'material-ui';

type StyleClasses = 
  'container'
  | 'userSmallAvatar' ;

const styles: StyleRulesCallback<StyleClasses> = (theme) => ({
  container: {
    padding: 16,
    background: theme.palette.grey['300'],
    textAlign: 'left',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    flex: 1
  },
  userSmallAvatar: {
    height: 36,
    width: 36,
    borderRadius: '50%',
    marginRight: 16
  },
});

interface Props {
  activeUserPhoto: string;
  onSubmitComment: (value: string) => void;
}

interface State {
  inputValue: string;
}

type PropsWithStyles = Props & WithStyles<StyleClasses>;

class CommentInput extends React.PureComponent<PropsWithStyles, State> {

  public state: State = {
    inputValue: ''
  };

  updateValue = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    this.setState({
      inputValue: e.target.value
    });
  }

  submitComment = () => {
    if (this.state.inputValue) {
      this.props.onSubmitComment(this.state.inputValue);
      this.setState({ inputValue: '' });
    }
  }

  public render() {
    return (
      <div className={this.props.classes.container}>
        <img className={this.props.classes.userSmallAvatar} src={this.props.activeUserPhoto} />
        <Input
          onChange={(e) => this.updateValue(e)}
          value={this.state.inputValue}
          placeholder={'Write a comment...'} 
          style={{ flex: 1, marginRight: 16 }} 
        />
        <IconButton onClick={() => this.submitComment()}>
          <Icon>send</Icon>
        </IconButton>
      </div>
    );
  }
}

export default withStyles(styles)<Props>(CommentInput);