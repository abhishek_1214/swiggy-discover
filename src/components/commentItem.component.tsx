import * as React from 'react';
import { 
  WithStyles, 
  StyleRulesCallback, 
  withStyles, 
  Typography, 
  Collapse, 
  IconButton, 
  Icon, 
  TextField, 
  ButtonBase
} from 'material-ui';
import { AppState, AppActions } from '../redux';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ReplyCreateRequest } from '../redux/reply/reply.actions';
import { PostUser } from '../redux/posts/posts.model';
import { firestore } from 'firebase';
import firebase from '../config/firebase';
import { Reply } from '../redux/reply/reply.model';

type StyleClasses = 
  'commentItemContainer'
  | 'avatar' 
  | 'commentText'
  | 'userNameText'
  | 'replyText' 
  | 'replyExpandContainer' 
  | 'bootstrapInput' 
  | 'bootstrapRoot' 
  | 'formLabel' 
  | 'replyContainer'
  | 'replyItemContainer' 
  | 'replyButtonBase' ;

const styles: StyleRulesCallback<StyleClasses> = (theme) => ({
  commentItemContainer: {
    padding: 16,
    alignItems: 'felx-start',
    display: 'flex',
    flexDirection: 'row'
  },
  replyItemContainer: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    margin: '8px 0'
  },
  avatar: {
    height: 30,
    width: 30,
    borderRadius: '50%',
    marginRight: 12
  },
  commentText: {
    color: theme.palette.grey['600']
  },
  userNameText: {
    color: theme.palette.grey['800'],
    fontWeight: 500 
  },
  replyText: {
    fontSize: 14,
    color: theme.palette.primary.main,
    fontWeight: 500
  },
  replyExpandContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  replyContainer: {
    display: 'flex',
    flexDirection: 'column',
    padding: '16px 4px'
  },
  bootstrapRoot: {
    padding: 0,
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    },
  },
  bootstrapInput: {
    borderRadius: 4,
    backgroundColor: theme.palette.common.white,
    border: '1px solid #c3c3c3',
    fontSize: 14,
    padding: '10px 12px',
    width: 'calc(100% - 24px)',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
  formLabel: {
    fontSize: 16
  },
  replyButtonBase: {
    width: '100%',
    textAlign: 'left',
    padding: '8px 4px',
    justifyContent: 'flex-start'
  }
});

interface Props {
  commentRef: string;
  activeUserPhoto: string;
  user: PostUser;
  comment: string;
}

interface ReduxProps {
  replyCreate: (payload: {
    commentRef: string;
    message: string;
  }) => ReplyCreateRequest;
}

interface State {
  replyInputValue: string;
  replyExpanded: boolean;
  replysRef: firestore.Query;
  replyRootExpanded: boolean;
  replies: Reply[];
}

type PropsWithStyle = Props & WithStyles<StyleClasses>;

type PropsWithStyleRedux = PropsWithStyle & ReduxProps;

class CommentItem extends React.PureComponent<PropsWithStyleRedux, State> {
  public unsubscribe: any;

  public constructor(props: PropsWithStyleRedux) {
    super(props);
    this.state = {
      replyInputValue: '',
      replyExpanded: false,
      replyRootExpanded: false,
      replysRef: 
        firebase
          .firestore
          .collection('/reply')
          .where('commentRef', '==', props.commentRef)
          .orderBy('createdAt'),
      replies: []
    };
    this.unsubscribe = this.state.replysRef.onSnapshot(query => this.onDocumentUpdate(query));
  }

  public onDocumentUpdate(query: firestore.QuerySnapshot) {
    let replies: Reply[] = [];
    query.forEach(item => replies.push(item.data() as Reply));

    this.setState({ replies });
  }

  public componentWillReceiveProps(nextProps: PropsWithStyleRedux) {
    if (nextProps.commentRef) {
      this.setState(
        {
          replysRef: 
            firebase
              .firestore
              .collection('/reply')
              .where('commentRef', '==', nextProps.commentRef)
              .orderBy('createdAt'),
        }, 
        () => {
          this.unsubscribe = this.state.replysRef.onSnapshot(query => this.onDocumentUpdate(query));
        }
      );
      
    }
  }

  public componentWillUnmount() {
    this.unsubscribe();
  }

  public updateInputValue(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ replyInputValue: e.target.value });
  }

  public toggleReplyExpander() {
    this.setState({ replyExpanded: !this.state.replyExpanded });
  }

  public toggleReplyRootExpander() {
    if (this.state.replies.length > 0) {
      this.setState({ replyRootExpanded: !this.state.replyRootExpanded });
    } else if (this.props.activeUserPhoto) {
      this.setState({ 
        replyRootExpanded: !this.state.replyRootExpanded,
        replyExpanded: !this.state.replyExpanded
      });
    }
  }

  public createReply() {
    if (this.state.replyInputValue && this.props.commentRef) {
      this.props.replyCreate({
        commentRef: this.props.commentRef,
        message: this.state.replyInputValue
      });
      this.setState({ replyInputValue: '' });
    }
  }

  public render() {
    return (
      <div className={this.props.classes.commentItemContainer}>
        <img src={this.props.user.photoURL} className={this.props.classes.avatar} />
        <div style={{ flex: 1 }}>
          <Typography 
            variant={'body1'} 
            className={this.props.classes.commentText} 
          >
            <span className={this.props.classes.userNameText}>{this.props.user.displayName}</span>: {this.props.comment}
          </Typography>
          {!this.state.replyRootExpanded ?
            <ButtonBase 
              onClick={() => this.toggleReplyRootExpander()}
              className={this.props.classes.replyButtonBase}
            >
              <Typography className={this.props.classes.replyText}>
                {this.state.replies.length > 0 ?
                  `View ${this.state.replies.length} Replies...`
                  : this.props.activeUserPhoto ? `Reply` : undefined
                }
              </Typography>
            </ButtonBase>
          : undefined}
          <Collapse in={this.state.replyRootExpanded}>
            <div className={this.props.classes.replyContainer}>
              {this.state.replies.map((reply, index) =>
                <div key={index} className={this.props.classes.replyItemContainer}>
                  <img src={reply.user.photoURL} className={this.props.classes.avatar} />
                  <Typography variant={'body1'} className={this.props.classes.commentText}>
                    <span className={this.props.classes.userNameText}>
                      {reply.user.displayName}
                    </span>
                    : {reply.message}
                  </Typography>
                </div>
              )}
            </div>
            {(!this.state.replyExpanded && this.props.activeUserPhoto) ? 
              <Typography 
                onClick={() => this.toggleReplyExpander()}
                variant={'body2'}
                className={this.props.classes.replyText}
              >
                Reply
              </Typography>
            : undefined}
            <Collapse in={this.state.replyExpanded}>
              <div className={this.props.classes.replyExpandContainer}>
                <img src={this.props.activeUserPhoto} className={this.props.classes.avatar} />
                <TextField 
                  InputProps={{
                    disableUnderline: true,
                    classes: {
                      root: this.props.classes.bootstrapRoot,
                      input: this.props.classes.bootstrapInput,
                    },
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: this.props.classes.formLabel,
                  }}
                  onChange={e => this.updateInputValue(e)}
                  value={this.state.replyInputValue}
                  style={{ flex: 1 }} 
                  placeholder={'Write a reply...'} 
                />
                <IconButton onClick={() => this.createReply()}>
                  <Icon>send</Icon>
                </IconButton>
              </div>
            </Collapse>
          </Collapse>
        </div>
      </div>
    );
  }
}

const StyledComponent = withStyles(styles)<Props>(CommentItem);

const mapStateToProps = (state: AppState) => ({
});

const mapDispatchToProps = (dispatch: Dispatch<AppActions>) =>
  bindActionCreators(
  {
    replyCreate: ReplyCreateRequest.create
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(StyledComponent); 