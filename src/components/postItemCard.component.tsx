import *  as React from 'react';
import { 
  StyleRulesCallback,
  WithStyles,
  withStyles,
  Paper,
  Typography,
  IconButton,
  Icon,
  Button,
} from 'material-ui';
import * as moment from 'moment';
import { PostUser } from '../redux/posts/posts.model';
import CommentInput from './commentInput.component';
import CommentsList from './comentsList.component';

type StyleClasses = 
    'container' 
  | 'postMessage' 
  | 'headerContainer' 
  | 'userAvatar'
  | 'userInfoContainer' 
  | 'timestamp' ;

const styles: StyleRulesCallback<StyleClasses> = (theme) => ({
  container: {
    width: 'calc(44vw - 24px)',
    flexDirection: 'column',
    margin: 16,
    [theme.breakpoints.down('lg')]: {
      width: 'calc(100vw - 32px)',
    }
  },
  postMessage: {
    padding: '8px 16px',
    textAlign: 'left',
    margin: '16px 0'
  },
  headerContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
  },
  userAvatar: {
    height: 48,
    width: 48,
    borderRadius: '50%',
    marginRight: 16
  },
  userInfoContainer: {
    textAlign: 'left',
    flex: 1
  },
  timestamp: {
    padding: 16,
    textAlign: 'right'
  }
});

interface Props {
  id: string;
  activeUserPhoto: undefined | string;
  likes: PostUser[];
  createdAt: string;
  user: PostUser;
  message: string;
  onSubmitComment: (comment: string) => void;
}

type PropsWithStyles = Props & WithStyles<StyleClasses>;

const getDisplayAge = (createdAt: string) => {
  return moment(createdAt).fromNow();
};

const PostItemCard: React.SFC<PropsWithStyles> = props => (
  <Paper elevation={2} className={props.classes.container}>
    <div className={props.classes.headerContainer}>
      <img className={props.classes.userAvatar} src={props.user.photoURL} />
      <div className={props.classes.userInfoContainer}>
        <Typography gutterBottom={false} variant={'body2'}>
          {props.user.displayName}
        </Typography>
        <Typography variant={'caption'}>
          {getDisplayAge(props.createdAt)}
        </Typography>
      </div>
      {props.likes.length > 0 ? (
        <Button>
          <Icon>favorite</Icon>
        </Button>
      ) : (
        <IconButton>
          <Icon>favorite_border</Icon>
        </IconButton>  
      )}
    </div>
    <Typography variant={'subheading'} className={props.classes.postMessage}>
      {`" ${props.message} "`}
    </Typography>
    <CommentsList activeUserPhoto={props.activeUserPhoto ? props.activeUserPhoto : ''} docRef={props.id} />
    {
      props.activeUserPhoto !== undefined ? 
        <CommentInput 
          onSubmitComment={val => props.onSubmitComment(val)}
          activeUserPhoto={props.activeUserPhoto} 
        /> 
        : undefined 
    }
  </Paper>
);

export default withStyles(styles)<Props>(PostItemCard); 