import { 
  AppBar,
  Button,
  Icon,
  StyleRulesCallback, 
  Toolbar,
  Typography,
  WithStyles,
  withStyles
} from 'material-ui';
import * as React from 'react';
import logo from '../logo.svg';
import { FirebaseGoogleUser } from '../models/firebase.auth.model';

type StyleClasses = 
    'navLogo' 
  | 'flex' 
  | 'buttonIcon' 
  | 'loginButton' 
  | 'userInfocontainer'
  | 'userAvatar'
  | 'userAvatarText';

const styles: StyleRulesCallback<StyleClasses> = (theme) => ({
  buttonIcon: {
    color: '#777777',
    marginRight: 8,
  },
  flex: {
    flex: 1,
    textAlign: 'left'
  },
  loginButton: {
    color: '#777777'
  },
  navLogo: {
    marginRight: 16,
    maxHeight: 36,
  },
  userInfocontainer: {
    padding: '8px 16px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  userAvatar: {
    width: 36,
    height: 36,
    borderRadius: '50%',
    marginRight: 8
  },
  userAvatarText: {
    color: theme.palette.grey['700'],
    [theme.breakpoints.down('lg')]: {
      display: 'none'
    }
  }
});

interface Props {
  title: string;
  user: undefined | FirebaseGoogleUser;
  onLoginPress: (e: React.MouseEvent<HTMLElement>) => void;
}

type PropsWithStyles = Props & WithStyles<StyleClasses>;

const NavBar: React.SFC<PropsWithStyles> = props => (
  <AppBar position={'static'} color={'default'}>
    <Toolbar>
      <img className={props.classes.navLogo} src={logo} />
      <Typography variant="title" color="inherit" className={props.classes.flex}>
        {props.title}
      </Typography>
      <div>
        { props.user === undefined ? (
          <Button onClick={(e) => props.onLoginPress(e)}>
            <Icon className={props.classes.buttonIcon}>account_circle</Icon>
            Login
          </Button>
        ) : (
          <div className={props.classes.userInfocontainer}>
            <img className={props.classes.userAvatar} src={props.user.photoURL} alt={props.user.displayName}/>
            <Typography variant={'body2'} className={props.classes.userAvatarText}>
              {props.user.displayName}
            </Typography>
          </div>
        )}
      </div>
    </Toolbar>
  </AppBar>
);

export default withStyles(styles)<Props>(NavBar);