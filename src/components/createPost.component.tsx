import * as React from 'react';
import { StyleRulesCallback, WithStyles, Paper, Input, withStyles, Button, colors } from 'material-ui';

type StyleClasses = 'container' | 'createButton';

const styles: StyleRulesCallback<StyleClasses> = (theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    width: 'calc(44vw - 24px)',
    padding: '24px 16px',
    [theme.breakpoints.down('lg')]: {
      width: 'calc(100vw - 32px)',
    }
  },
  createButton: {
    marginTop: 24,
    maxWidth: 420,
    alignSelf: 'flex-end',
    color: colors.grey['100']
  }
});

interface Props {
  onCreatePost: (message: string) => void;
}

interface State {
  message: string;
}

type PropsWithStyles = Props & WithStyles<StyleClasses>;

class CreatePost extends React.PureComponent<PropsWithStyles, State> {

  public state: State = {
    message: ''
  };

  public updateMessageState(message: string) {
    this.setState({ message });
  }

  public handleClick() {
    if (this.state.message !== '') {
      this.props.onCreatePost(this.state.message);
      this.setState({ message: '' });
    }
  }

  public render() {
    return (
        <Paper elevation={1} className={this.props.classes.container}>
          <Input 
            value={this.state.message}
            placeholder={'Write something'}
            onChange={e => this.updateMessageState(e.target.value)} 
          />
          <Button 
            onClick={() => this.handleClick()}
            color={'primary'}
            className={this.props.classes.createButton}
            variant={'raised'}
          >
            CREATE POST
          </Button>
        </Paper>
    );
  }
}

export default withStyles(styles)<Props>(CreatePost);