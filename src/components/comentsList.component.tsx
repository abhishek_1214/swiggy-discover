import * as React from 'react';
import { 
  WithStyles,
  withStyles,
  StyleRulesCallback,
} from 'material-ui';
import { firestore } from 'firebase';
import firebase from '../config/firebase';
import { CommentDocument } from '../redux/comments/comments.model';
import CommentItem from './commentItem.component';

type StyleClasses = 
  'container';

const styles: StyleRulesCallback<StyleClasses> = (theme) => ({
  container: {
    width: '100%',
    paddingTop: 16,
    background: theme.palette.grey['300'],
    textAlign: 'left',
    display: 'flex',
    flexDirection: 'column'
  }
});

interface State {
  comments: CommentDocument[];
  commentsRef: firestore.Query;
}

interface Props {
  activeUserPhoto: string;
  docRef: string;
}

type PropsWithStyles = Props & WithStyles<StyleClasses>;

class CommentsList extends React.PureComponent<PropsWithStyles, State> {
  public unsubscribe: any;

  public constructor(props: PropsWithStyles) {
    super(props);
    this.state = {
      comments: [],
      commentsRef: 
        firebase
          .firestore
          .collection('/comments')
          .where('postId', '==', props.docRef)
          .orderBy('createdAt'),
    };
    this.unsubscribe = this.state.commentsRef.onSnapshot(query => this.onDocumentUpdate(query));
  }

  public onDocumentUpdate(query: firestore.QuerySnapshot) {
    let comments: CommentDocument[] = [];
    query.forEach(item => comments.push({
      id: item.id,
      ...item.data() as CommentDocument
    }));
    this.setState({
      comments
    });
  }

  public componentWillReceiveProps(nextProps: PropsWithStyles) {
    if (nextProps.docRef) {
      this.setState(
        {
          commentsRef: 
            firebase
              .firestore
              .collection('/comments')
              .where('postId', '==', nextProps.docRef)
              .orderBy('createdAt'),
        }, 
        () => {
          this.unsubscribe = this.state.commentsRef.onSnapshot(query => this.onDocumentUpdate(query));
        }
      );
      
    }
  }

  public componentWillUnmount() {
    this.unsubscribe();
  }

  public render() {
    return (
      <div>
        {this.state.comments.length > 0 ? (
          <div className={this.props.classes.container}>
            {this.state.comments.map((item, index) =>
              <CommentItem
                key={index.toString()}
                activeUserPhoto={this.props.activeUserPhoto}
                commentRef={item.id ? item.id : ''}
                user={item.user}
                comment={item.comment}
              />
            )}
          </div>
        ) : undefined}
      </div>
    );
  }
}

export default withStyles(styles)<Props>(CommentsList);