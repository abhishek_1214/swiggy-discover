import withStyles, { StyleRulesCallback, WithStyles } from 'material-ui/styles/withStyles';
import * as React from 'react';
import NavBar from '../../components/navbar.component';
import CreatePost from '../../components/createPost.component';
import PostItemCard from '../../components/postItemCard.component';
import withRoot from '../../utils/withRoot';
import AuthState from '../../redux/auth/auth.model';
import firebaseApp from '../../config/firebase';
import { firestore } from 'firebase';
import { PostItem } from '../../redux/posts/posts.model';
import { Grid } from 'material-ui';

type StyleClasses = 'page' | 'content';

const styles: StyleRulesCallback<StyleClasses> = theme => ({
  page: {
    background: '#eeeeee',
    height: '100vh',
    textAlign: 'center',
  },
  content: {
    padding: '36px 11%'
  }
});

interface Props {
  auth: AuthState;
  name: string;
  postCreate: (payload: { message: string }) => void;
  authRequest: (payload: {}) => void;
  commentCreate: (payload: {
    comment: string,
    post: PostItem
  }) => void;
}

interface State {
  open: boolean;
  posts: PostItem[];
}

type PropsWithStyle = Props & WithStyles<StyleClasses>;

class HomeComponent extends React.Component<PropsWithStyle, State> {
  public postsRef: firestore.CollectionReference;
  public unsubscribe: any;
  public state: State = {
    open: false,
    posts: []
  };

  public constructor(props: PropsWithStyle) {
    super(props);
    this.postsRef = firebaseApp.firestore.collection('/posts');
    this.unsubscribe = null;
  }

  public componentDidMount() {
    this.unsubscribe = this.postsRef.orderBy('createdAt', 'desc').onSnapshot(query => this.onCollectionUpdate(query));
  }

  public componentWillUnmount() {
    this.unsubscribe();
  }

  public onCollectionUpdate(querySnapshot: firestore.QuerySnapshot) {
    let posts: PostItem[] = [];
    querySnapshot.forEach(item => {
      posts.push(new PostItem({
        id: item.id,
        ...item.data() as PostItem
      }));
    });

    this.setState({ posts: posts });
  }

  public createPost(message: string) {
    if (message) {
      this.props.postCreate({ message });
    }
  }

  public handleLogin(e: React.MouseEvent<HTMLElement>) {
    e.preventDefault();
    this.props.authRequest({});
  }

  public addCommentToPost(comment: string, post: PostItem) {
    this.props.commentCreate({
      comment,
      post
    });
  }

  public render() {
    return (
      <div className={this.props.classes.page}>
        <NavBar
          user={this.props.auth.toJS().user}
          title={'Discover'}
          onLoginPress={e => this.handleLogin(e)} 
        />
        <Grid 
          direction={'column'} 
          alignItems={'center'} 
          spacing={0} 
          container={true} 
          className={this.props.classes.content}
        >
          { this.props.auth.user === undefined ? 
              undefined :
              <CreatePost onCreatePost={message => this.createPost(message)} />
          }
          { this.state.posts.map((item, postIndex) =>
            <PostItemCard
              id={item.id ? item.id : ''}
              onSubmitComment={comment => this.addCommentToPost(comment, item)}
              activeUserPhoto={this.props.auth.user ? this.props.auth.user.photoURL : undefined}
              likes={item.likes}
              user={item.user}
              createdAt={item.createdAt}
              key={postIndex}
              message={item.message} 
            />
          )}
        </Grid>
      </div>
    );
  }
}

export default withRoot(withStyles(styles)<Props>(HomeComponent));