import HomeComponent from './home.component';
import { connect } from 'react-redux';
import { Dispatch, bindActionCreators } from 'redux';
import { AuthRequestAction } from 'src/redux/auth/auth.actions';
import { AppActions, AppState } from '../../redux';
import { PostCreateRequest } from '../../redux/posts/posts.actions';
import { CommentCreateRequest } from '../../redux/comments/comments.actions';

const mapStateToProps = (state: AppState) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch: Dispatch<AppActions>) =>
  bindActionCreators(
  {
    authRequest: AuthRequestAction.create,
    postCreate: PostCreateRequest.create,
    commentCreate: CommentCreateRequest.create
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);