interface ActionCreatorProps {
  type: string;
  payload: any;
}

const typedActionCreator = (props: ActionCreatorProps) => {
  class TypedAction {
    readonly type = props.type;
    static create(params: typeof props.payload) {
      return Object.assign({}, new TypedAction(params));
    }
    public constructor(public payload: typeof props.payload) {}
  }

  return TypedAction;
};
  