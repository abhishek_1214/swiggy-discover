import { fromPromise } from 'rxjs/observable/fromPromise';
import firebase from '../config/firebase';
import { CommentDocument } from '../redux/comments/comments.model';
import { Reply } from '../redux/reply/reply.model';

class FirebaseUtils {
  createNewComment = (comment: CommentDocument) => {
    return fromPromise(firebase.firestore.collection('/comments').add(comment));
  }

  createNewReply = (reply: Reply) => {
    return fromPromise(firebase.firestore.collection('/reply').add(reply));
  }
}

export default FirebaseUtils;