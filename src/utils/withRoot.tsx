import * as colors from 'material-ui/colors';
import CssBaseline from 'material-ui/CssBaseline';
import { createMuiTheme, MuiThemeProvider } from 'material-ui/styles';
import * as React from 'react';
import { ThemeOptions } from 'material-ui/styles/createMuiTheme';

// A theme with custom primary and secondary color.
// It's optional.
const themeOptions: ThemeOptions = {
  palette: {
    type: 'light',
    primary: colors.orange,
    secondary: colors.yellow,
  },
};

const theme = createMuiTheme(themeOptions);

function withRoot(Component: React.ComponentType) {
  function WithRoot(props: object) {
    // MuiThemeProvider makes the theme available down the React tree
    // thanks to React context.
    return (
      <MuiThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Component {...props} />
      </MuiThemeProvider>
    );
  }

  return WithRoot;
}

export default withRoot;