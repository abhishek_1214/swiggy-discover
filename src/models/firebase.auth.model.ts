export interface FirebaseAuthResponse {
  user: FirebaseGoogleUser;
  credential: Credential;
  additionalUserInfo: AdditionalUserInfo;
  operationType: string;
}

interface AdditionalUserInfo {
  providerId: string;
  isNewUser: boolean;
  profile: Profile;
}

interface Profile {
  id: string;
  email: string;
  verified_email: boolean;
  name: string;
  given_name: string;
  family_name: string;
  link: string;
  picture: string;
  gender: string;
  locale: string;
}

interface Credential {
  idToken: string;
  accessToken: string;
  providerId: string;
  signInMethod: string;
}

export interface FirebaseGoogleUser {
  uid: string;
  displayName: string;
  photoURL: string;
  email: string;
  emailVerified: boolean;
  phoneNumber?: any;
  isAnonymous: boolean;
  providerData: ProviderDatum[];
  apiKey: string;
  appName: string;
  authDomain: string;
  stsTokenManager: StsTokenManager;
  redirectEventId?: any;
  lastLoginAt: string;
  createdAt: string;
}

interface StsTokenManager {
  apiKey: string;
  refreshToken: string;
  accessToken: string;
  expirationTime: number;
}

interface ProviderDatum {
  uid: string;
  displayName: string;
  photoURL: string;
  email: string;
  phoneNumber?: any;
  providerId: string;
}