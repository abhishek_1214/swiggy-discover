import { ActionTypes, AppPersistedState } from '../redux';

class PersistAction {
  key: string;
  readonly type = ActionTypes.REHYDRATE;
  public payload: AppPersistedState;
}

export default PersistAction;