import { createStore, applyMiddleware, compose } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { createLogger } from 'redux-logger';
import redux from './';
import { createEpicMiddleware } from 'redux-observable';

const epicMiddleware = createEpicMiddleware(redux.epics);
const logger = createLogger({ collapsed: true });

const rootPersisConfig = {
  key: 'root',
  storage: storage
};

const persistedReducer = persistReducer(rootPersisConfig, redux.reducers);

let middleware = [logger, epicMiddleware];

const configureStore = () => {
  let store = createStore(
    persistedReducer,
    compose(
      applyMiddleware(...middleware)
    )
  );

  let persistor = persistStore(store);

  return { store, persistor };
};

export default configureStore;