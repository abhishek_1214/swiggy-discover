import { ActionTypes } from '..';
import { FirebaseGoogleUser } from '../../models/firebase.auth.model';

export class AuthRequestAction {
  readonly type = ActionTypes.AUTH_REQUEST;
  static create(payload: {}) {
    return Object.assign({}, new AuthRequestAction(payload));
  }
  public constructor(public payload: {}) {}
}

export class AuthReceiveAction {
  readonly type = ActionTypes.AUTH_RECIEVE;
  static create(payload: {
    token: string;
    user: FirebaseGoogleUser;
  }) {
    return Object.assign({}, new AuthReceiveAction(payload));
  }
  public constructor(public payload: {
    token: string;
    user: FirebaseGoogleUser;
  }) {}
}

export class AuthErrorAction {
  readonly type = ActionTypes.AUTH_ERROR;
  static create(payload: {
    error: any;
  }) {
    return Object.assign({}, new AuthErrorAction(payload));
  }
  public constructor(public payload: {
    error: any;
  }) {}
}

export type AuthActions = 
    AuthRequestAction
  | AuthReceiveAction
  | AuthErrorAction;