import { Epic, combineEpics } from 'redux-observable';
import { AppActions, AppState, ActionTypes } from '..';
import firebase from '../../config/firebase';
import { AuthReceiveAction, AuthErrorAction } from './auth.actions';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { Observable } from 'rxjs/Observable';
import { FirebaseAuthResponse } from '../../models/firebase.auth.model';

const authRequestEpic: Epic<AppActions, AppState> = (action$, store) =>
  action$
    .ofType(ActionTypes.AUTH_REQUEST)
    .mergeMap(action =>
      fromPromise(firebase.app.auth().signInWithPopup(firebase.provider))
        .map((res: FirebaseAuthResponse) => {
          return AuthReceiveAction.create({
            token: res.credential.accessToken,
            user: res.user
          });
        })
        .catch(err => Observable.of(AuthErrorAction.create({ error: err })))
    );

export default combineEpics(authRequestEpic);