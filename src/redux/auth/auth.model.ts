import { Record } from 'immutable';
import { FirebaseGoogleUser } from '../../models/firebase.auth.model';

export interface IAuthState {
  token: string;
  user: FirebaseGoogleUser | undefined;
}

const authState = Record({
  token: '',
  user: undefined
});

class AuthState extends authState implements IAuthState {
  token: string;
  user: FirebaseGoogleUser;
  
  public constructor(props: IAuthState) {
    super(props);
  }

  with(props: IAuthState) {
    return this.merge(props) as this;
  }
}

export default AuthState;