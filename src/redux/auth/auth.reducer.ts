import AuthState from './auth.model';
import { Reducer } from 'redux';
import { ActionTypes, AppActions } from '..';

const inital = new AuthState({
  token: '',
  user: undefined
});

const authReducer: Reducer<AuthState> = (state = inital, action: AppActions) => {
  switch (action.type) {
    case ActionTypes.AUTH_RECIEVE:
      return state.with({
        token: action.payload.token,
        user: action.payload.user
      });
    case ActionTypes.REHYDRATE:
      if (action.payload && action.payload.auth) { return new AuthState(action.payload.auth); }
      return inital;
    default:
      return state;
  }
};

export default authReducer;