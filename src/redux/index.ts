import { combineReducers } from 'redux';
import authReducer from './auth/auth.reducer';
import { AuthActions } from './auth/auth.actions';
import AuthState, { IAuthState } from './auth/auth.model';
import { combineEpics } from 'redux-observable';
import authEpic from './auth/auth.epic';
import PersistAction from '../models/persistAction.model';
import PostsState, { IPostsState } from './posts/posts.model';
import postsReducer from './posts/posts.reducer';
import { PostActions } from './posts/posts.actions';
import postsEpic from './posts/posts.epic';
import { CommentsActions } from './comments/comments.actions';
import commentsEpic from './comments/comments.epic';
import { ReplyActions } from './reply/reply.actions';
import replyEpics from './reply/reply.epics';

export enum ActionTypes {
  // Auth Actions
  AUTH_REQUEST = 'AUTH_REQUEST',
  AUTH_RECIEVE = 'AUTH_RECIEVE',
  AUTH_ERROR = 'AUTH_ERROR',

  // Comments Actions
  COMMENT_CREATE_REQUEST = 'COMMENT_CREATE_REQUEST',
  COMMENT_CREATE_RECIEVE = 'COMMENT_CREATE_RECIEVE',
  COMMENT_CREATE_ERROR  = 'COMMENT_CREATE_ERROR',

  // Posts Actions
  POSTS_CREATE_REQUEST = 'POSTS_CREATE_REQUEST',
  POSTS_CREATE_RECIEVE = 'POSTS_CREATE_RECIEVE',
  POSTS_CREATE_ERROR = 'POSTS_CREATE_ERROR',

  // Reply Actions
  REPLY_CREATE_REQUEST = 'REPLY_CREATE_REQUEST',
  REPLY_CREATE_RECIEVE = 'REPLY_CREATE_RECIEVE',
  REPLY_CREATE_ERROR = 'REPLY_CREATE_ERROR',

  REHYDRATE = 'persist/REHYDRATE'
}

export type AppActions =
  AuthActions
  | PostActions
  | PersistAction
  | CommentsActions
  | ReplyActions ;

export type AppState = {
  auth: AuthState,
  posts: PostsState
};

export type AppPersistedState = {
  auth: IAuthState;
  posts: IPostsState;
  _persist: {
    rehydrated: boolean;
    version: number;
  }
};

export default {
  reducers: combineReducers({
    auth: authReducer,
    posts: postsReducer
  }),
  epics: combineEpics(
    authEpic,
    postsEpic,
    commentsEpic,
    replyEpics
  )
};