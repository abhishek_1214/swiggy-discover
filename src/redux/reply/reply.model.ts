import { PostUser } from '../posts/posts.model';

export class Reply {
  commentRef: string;
  message: string;
  user: PostUser;
  createdAt: string;
}