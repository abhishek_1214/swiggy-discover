import { Epic, combineEpics } from 'redux-observable';
import { AppActions, AppState, ActionTypes } from '..';
import { Observable } from 'rxjs/Observable';
import FirebaseUtils from '../../utils/firebaseUtils';
import { Reply } from './reply.model';
import { ReplyCreateRequest, ReplyCreateRecieve, ReplyCreateError } from './reply.actions';
import * as moment from 'moment';

const createReplyEpic: Epic<AppActions, AppState> = (action$, store) =>
  action$
    .ofType(ActionTypes.REPLY_CREATE_REQUEST)
    .mergeMap((action: ReplyCreateRequest) => {
      let userRef = store.getState().auth.user;
      let reply: Reply = {
        commentRef: action.payload.commentRef,
        message: action.payload.message,
        user: {
          email: userRef.email,
          displayName: userRef.displayName,
          photoURL: userRef.photoURL,
          uid: userRef.uid
        },
        createdAt: moment().toISOString()
      };
      return Observable.of(new FirebaseUtils().createNewReply(reply))
        .map(res => ReplyCreateRecieve.create({ message: 'Success' }))
        .catch(err => Observable.of(ReplyCreateError.create({ error: err})));
    });

export default combineEpics(createReplyEpic);