import { ActionTypes } from '..';

export class ReplyCreateRequest {
  readonly type = ActionTypes.REPLY_CREATE_REQUEST;
  static create(payload: {
    commentRef: string;
    message: string;
  }) {
    return Object.assign({}, new ReplyCreateRequest(payload));
  }
  constructor(public payload: {
    commentRef: string;
    message: string;
  }) {}
}

export class ReplyCreateRecieve {
  readonly type = ActionTypes.REPLY_CREATE_RECIEVE;
  static create(payload: {
    message: string,
  }) {
    return Object.assign({}, new ReplyCreateRecieve(payload));
  }
  constructor(public payload: {
    message: string,
  }) {}
}

export class ReplyCreateError {
  readonly type = ActionTypes.REPLY_CREATE_REQUEST;
  static create(payload: {
    error: any,
  }) {
    return Object.assign({}, new ReplyCreateError(payload));
  }
  constructor(public payload: {
    error: string,
  }) {}
}

export type ReplyActions =
  ReplyCreateRequest
  | ReplyCreateRecieve
  | ReplyCreateError ;