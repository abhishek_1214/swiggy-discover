import { Epic, combineEpics } from 'redux-observable';
import { AppActions, AppState, ActionTypes } from '..';
import * as moment from 'moment';
import firebase from '../../config/firebase';
import { PostItem } from './posts.model';
import { PostCreateRequest, PostsCreateRecieve, PostsCreateError } from './posts.actions';
import { Observable } from 'rxjs/Observable';
import { FirebaseGoogleUser } from '../../models/firebase.auth.model';

const createPostEpic: Epic<AppActions, AppState> = (action$, store) =>
  action$
    .ofType(ActionTypes.POSTS_CREATE_REQUEST)
    .mergeMap((action: PostCreateRequest) => {
      let user: FirebaseGoogleUser = store.getState().auth.toJS().user;
      var post: PostItem = {
        message: action.payload.message,
        createdAt: moment().toISOString(),
        user: {
          email: user.email,
          displayName: user.displayName,
          photoURL: user.photoURL,
          uid: user.uid
        },
        likes: []
      };
      return Observable.of(firebase.firestore.collection('/posts').add(post))
        .map(res => PostsCreateRecieve.create({ message: '' }))
        .catch(err => Observable.of(PostsCreateError.create({ error: err })));
    });

export default combineEpics(createPostEpic);