import PostsState from './posts.model';
import { List } from 'immutable';
import * as moment from 'moment';
import { Reducer } from 'redux';
import { AppActions, ActionTypes } from '..';

const initial = new PostsState({
  posts: List(),
  lastUpdated: moment()
});

const postsReducer: Reducer<PostsState> = (state = initial, action: AppActions) => {
  switch (action.type) {
    case ActionTypes.REHYDRATE:
      if (action.payload && action.payload.posts) { return state.with(action.payload.posts); }
      return state;
    default:
      return state;
  }
};

export default postsReducer;