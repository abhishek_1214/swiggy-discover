import { Record, List } from 'immutable';
import * as moment from 'moment';
import { Moment } from 'moment';

export interface PostUser {
  email: string;
  displayName: string;
  photoURL: string;
  uid: string;
}

export class PostItem {
  id?: string;
  message: string;
  createdAt: string;
  user: PostUser;
  likes: PostUser[];

  constructor(props: {
    id?: string;
    message: string;
    createdAt: string;
    user: PostUser;
    likes: PostUser[];
  }) {
    this.id = props.id;
    this.message = props.message;
    this.createdAt = props.createdAt;
    this.user = props.user;
    this.likes = props.likes;
  }
}

export interface IPostsState {
  posts: List<any>;
  lastUpdated: Moment;
}

const postsState = Record({
  posts: List(),
  lastUpdated: moment()
});

class PostsState extends postsState implements IPostsState {
  posts: List<any>;
  lastUpdated: Moment;

  with(props: IPostsState) {
    return this.merge(props) as this;
  }

  constructor(props: IPostsState) {
    super();
  }
}

export default PostsState;