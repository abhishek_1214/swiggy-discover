import { ActionTypes } from '..';

export class PostCreateRequest {
  readonly type = ActionTypes.POSTS_CREATE_REQUEST;
  static create(props: {
    message: string
  }) {
    return Object.assign({}, new PostCreateRequest(props));
  }
  public constructor(public payload: {
    message: string;
  }) {}
}

export class PostsCreateRecieve {
  readonly type = ActionTypes.POSTS_CREATE_RECIEVE;
  static create(props: {
    message: string;
  }) {
    return Object.assign({}, new PostsCreateRecieve(props));
  }
  public constructor(public payload: {
    message: string;
  }) {}
}

export class PostsCreateError {
  readonly type = ActionTypes.POSTS_CREATE_ERROR;
  static create(props: {
    error: any;
  }) {
    return Object.assign({}, new PostsCreateError(props));
  }
  public constructor(public payload: {
    error: any;
  }) {}
}

export type PostActions =
  PostCreateRequest
  | PostsCreateRecieve
  | PostsCreateError;