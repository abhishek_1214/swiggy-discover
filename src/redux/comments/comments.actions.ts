import { ActionTypes } from '..';
import { PostItem } from '../posts/posts.model';

export class CommentCreateRequest {
  readonly type = ActionTypes.COMMENT_CREATE_REQUEST;
  static create(payload: {
    comment: string,
    post: PostItem
  }) {
    return Object.assign({}, new CommentCreateRequest(payload));
  }
  constructor(public payload: {
    comment: string,
    post: PostItem
  }) {}
}

export class CommentCreateRecieve {
  readonly type = ActionTypes.COMMENT_CREATE_RECIEVE;
  static create(payload: {
    message: string,
  }) {
    return Object.assign({}, new CommentCreateRecieve(payload));
  }
  constructor(public payload: {
    message: string,
  }) {}
}

export class CommentCreateError {
  readonly type = ActionTypes.COMMENT_CREATE_RECIEVE;
  static create(payload: {
    error: any,
  }) {
    return Object.assign({}, new CommentCreateError(payload));
  }
  constructor(public payload: {
    error: string,
  }) {}
}

export type CommentsActions =
  CommentCreateRequest
  | CommentCreateRecieve
  | CommentCreateError ;