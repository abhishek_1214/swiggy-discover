import { PostUser } from '../posts/posts.model';

export class CommentDocument {
  id?: string;
  postId: string;
  user: PostUser;
  comment: string;
  createdAt: string;
}