import { Epic, combineEpics } from 'redux-observable';
import { AppActions, AppState, ActionTypes } from '..';
import { Observable } from 'rxjs/Observable';
import { CommentCreateError, CommentCreateRequest, CommentCreateRecieve } from './comments.actions';
import { CommentDocument } from './comments.model';
import * as moment from 'moment';
import FirebaseUtils from '../../utils/firebaseUtils';

const createCommentEpic: Epic<AppActions, AppState> = (action$, store) =>
  action$
    .ofType(ActionTypes.COMMENT_CREATE_REQUEST)
    .mergeMap((action: CommentCreateRequest) => {
      let docRef = action.payload.post.id ? action.payload.post.id : ''; 
      let userRef = store.getState().auth.user;
      let comment: CommentDocument = {
        postId: docRef,
        user: {
          email: userRef.email,
          displayName: userRef.displayName,
          photoURL: userRef.photoURL,
          uid: userRef.uid
        },
        comment: action.payload.comment,
        createdAt: moment().toISOString()
      };
      return new FirebaseUtils().createNewComment(comment)
        .map(res => CommentCreateRecieve.create({ message: 'created' }))
        .catch(err => Observable.of(CommentCreateError.create({ error: err })));
    });

export default combineEpics(createCommentEpic);