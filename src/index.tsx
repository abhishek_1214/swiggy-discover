import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'rxjs';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import configureStore from './redux/store';
import PublicRoutes from './routes';
import { PersistGate } from 'redux-persist/integration/react';

const persistedStore = configureStore();

ReactDOM.render(
  <Provider store={persistedStore.store}>
    <PersistGate loading={null} persistor={persistedStore.persistor}>
      <PublicRoutes />  
    </PersistGate>
  </Provider>
  ,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
