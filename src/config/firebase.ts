import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyAAwpZV3HCO2CZXomyROStu0ee-Mc1Waic',
  authDomain: 'swiggy-discover.firebaseapp.com',
  databaseURL: 'https://swiggy-discover.firebaseio.com',
  messagingSenderId: '917116603810',
  projectId: 'swiggy-discover',
  storageBucket: 'swiggy-discover.appspot.com',
};

const provider = new firebase.auth.GoogleAuthProvider();

const firebaseApp = firebase.initializeApp(config);

const firestoreSettings = { timestampsInSnapshots: true };

firebaseApp.firestore().settings(firestoreSettings);

export default {
  app: firebaseApp as firebase.app.App,
  provider: provider,
  firestore: firebaseApp.firestore()
};